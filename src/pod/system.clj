
(ns pod.system
  (:require [pod.web :refer [app]]
            [com.stuartsierra.component :as component]
            [org.httpkit.server :refer [run-server]]))

(defn- start-server [handler port]
  (let [server (run-server handler {:port port})]
    (println "running...")
    server))

(defn- stop-server [server]
  (when server
    (server)))


(defrecord pod []
  component/Lifecycle
  (start [this]
    (assoc this :server (start-server #'app 9009)))
  (stop [this]
    (stop-server (:server this))
    (dissoc this :server)))


(defn create-system []
  (pod.))

(defn -main [& args]
  (.start (create-system)))


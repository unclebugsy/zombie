

(defproject pod "0.1.0-SNAPSHOT"
  :description "what"

  :url ""
  :license {:name "GPL"
            :url "http://something.com/something"}
  
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.7.228"]

                 [http-kit "2.1.21-alpha2"]
                 [com.stuartsierra/component "0.3.1"]
                 [compojure "1.4.0"]
                 [quiescent "0.2.0-RC2"]
                 [jarohen/chord "0.7.0-SNAPSHOT"]
                 [org.clojure/core.async "0.2.374"]]

  :main pod.system
  :profiles {:dev {:plugins [[lein-cljsbuild "1.1.2"]
                             [lein-figwheel "0.5.0-6"]]
                   :dependencies [[reloaded.repl "0.2.1"]]
                   :source-paths ["dev"]
                   
                   :cljsbuild {:builds [{:source-paths ["src" "dev"]
                                         :figwheel true
                                         :compiler {:output-to "target/classes/public/app.js"
                                                    :output-dir "target/classes/public/out"
                                                    :main "pod.client"
                                                    :asset-path "/out"
                                                    :optimizations :none
                                                    :recompile-dependents true
                                                    :source-map true}}]}}})
